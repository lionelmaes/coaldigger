# coaldigger

A news scraper consisting of two distinct processes launched every 15 minutes by a cron job.
An instance of coaldigger is running online at coaldig.com since January 2014.

The two processes are:
* grab.py, which scans one or several news indexes (ex: google news) looking for article links, then looks for patterns in each of those articles to extract specific content (ex: title, header, body, images), then save the content in the database and download the images.
* analyse.py, which apply filters on the downloaded content and images to deduce more data from it (e.g. average colours from images - the only filter currently running)

![Grabber sequence diagram](sequence.svg)
