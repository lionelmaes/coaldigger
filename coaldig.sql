-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 10, 2019 at 02:03 PM
-- Server version: 10.1.37-MariaDB-0+deb9u1
-- PHP Version: 7.0.33-0+deb9u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coaldig`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `header` text NOT NULL,
  `text` text NOT NULL,
  `insert_date` datetime NOT NULL,
  `pub_date` datetime NOT NULL,
  `src_url` text NOT NULL,
  `author` text NOT NULL,
  `mine_ref` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `articles_rel`
--

CREATE TABLE `articles_rel` (
  `a_ref` int(10) UNSIGNED NOT NULL,
  `b_ref` int(10) UNSIGNED NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `extractor_img`
--

CREATE TABLE `extractor_img` (
  `id` int(10) UNSIGNED NOT NULL,
  `field` varchar(100) NOT NULL,
  `pattern` varchar(100) NOT NULL,
  `pattern_caption` varchar(100) NOT NULL,
  `mine_ref` int(11) NOT NULL,
  `post_treatment` text NOT NULL,
  `post_treatment_caption` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extractor_img`
--

INSERT INTO `extractor_img` (`id`, `field`, `pattern`, `pattern_caption`, `mine_ref`, `post_treatment`, `post_treatment_caption`) VALUES
(1, 'photo', 'article figure img', 'article figure figcaption', 1, 'result.get(\'src\')', 'result.text_content()'),
(2, 'photo', '.rtbf-article-main__img-cover img', 'span.www-media-caption__text', 3, 'result.get(\'src\')', 'result.text_content()'),
(3, 'photo', 'article .media img', 'article .media img', 4, 'result.get(\'src\')', 'result.get(\'title\')'),
(4, 'photo', '.article .illustration-img noscript img', '.article .illustration-img noscript img', 5, 'result.get(\'src\')', 'result.get(\'title\')'),
(5, 'photo', 'article figure img', 'article figure figcaption', 6, 'result.get(\'src\')', 'result.text_content()'),
(6, 'photo', 'article figure img', 'article figure figcaption', 7, 'result.get(\'src\')', 'result.text_content()'),
(7, 'photo', 'article #detail_content > img', 'article #detail_content > span.figcaption', 8, 'result.get(\'src\')', 'result.text_content()'),
(8, 'photo', '#article-photo meta[itemprop=\"image\"]', '#article-photo p', 9, 'result.get(\'content\')', 'result.text_content()'),
(9, 'photo', 'header figure img', 'header figure img', 10, 'self.addPrefix(result.get(\'src\'), \'http://www.lepoint.fr\') ', 'result.get(\'title\')'),
(10, 'photo', 'article figure img', 'article figure img', 11, 'result.get(\'src\')', 'result.get(\'alt\')'),
(11, 'photo', 'article .news-thumb img', 'article .news-thumb img', 12, 'result.get(\'src\')', 'result.get(\'title\')'),
(12, 'photo', 'header figure img', 'header figure figcaption', 13, 'result.get(\'src\')', 'result.text_content()'),
(13, 'photo', '#obs-article-mainpic img', '#obs-article-mainpic span', 14, 'result.get(\'src\')', 'result.text_content()'),
(14, 'photo', '.post-content p img', '', 15, 'result.get(\'src\')', ''),
(15, 'photo', '.article-header-photo img', '.aef-image-infos-title-legend', 16, 'result.get(\'src\')', 'result.text_content()'),
(16, 'photo', 'article .gr-main-image, article figure img', 'article div.gr-caption, article figure figcaption', 17, 'self.addPrefix(result.get(\'src\'), \'http://www.sudinfo.be\')', 'result.text_content()'),
(17, 'photo', 'article img.mainImage', 'article img.mainImage', 18, 'result.get(\'src\')', 'result.get(\'alt\')'),
(18, 'photo', 'article.article .slideshow__carousel li img', '.slideshow__meta .slideshow__image__credits', 19, 'self.removeQueryString(result.get(\'src\'))', 'result.text'),
(19, 'photo', 'div#main div.visual img', 'div#main div.visual img', 20, 'result.get(\'src\')', 'result.get(\'alt\')'),
(20, 'photo', 'article .w-content-details-article-img img', 'article .w-content-details-article-img img', 21, 'result.get(\'src\')', 'result.get(\'alt\')'),
(21, 'photo', 'article .media-inline-image img', '.pane-content .legend', 22, 'result.get(\'src\')', 'result.text_content()'),
(22, 'photo', '.illustration-img noscript img', '.illustration-img noscript img', 23, 'result.get(\'src\')', 'result.get(\'title\')'),
(23, 'photo', 'article .gr-main-media img', 'article div.gr-caption', 25, 'self.addPrefix(result.get(\'src\'), \'http://www.lesoir.be\')', 'result.text_content()'),
(24, 'photo', 'figure a', 'figure a', 26, 'result.get(\'href\')', 'result.get(\'data-caption\')'),
(25, 'photo', 'article figure img', 'article figure figcaption', 27, 'result.get(\'src\')', 'result.text_content()'),
(26, 'photo', 'article figure img', 'article figure figcaption', 28, 'result.get(\'src\')', 'result.text_content()'),
(27, 'photo', 'article>img', 'article .legende', 29, 'result.get(\'src\')', 'result.text_content()'),
(28, 'photo', 'article .gr-main-media img', 'article .gr-main-media img', 30, 'self.addPrefix(result.get(\'src\'), \'http://soirmag.lesoir.be\')', 'result.get(\'data-copyright\')'),
(29, 'photo', 'article header img.wp-post-image, article .o-post__content img.wp-post-image', 'article .c-post__credit', 31, 'result.get(\'src\')', 'result.text_content()');

-- --------------------------------------------------------

--
-- Table structure for table `extractor_text`
--

CREATE TABLE `extractor_text` (
  `id` int(10) UNSIGNED NOT NULL,
  `field` varchar(100) NOT NULL,
  `pattern` varchar(100) NOT NULL,
  `mine_ref` int(10) UNSIGNED NOT NULL,
  `post_treatment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extractor_text`
--

INSERT INTO `extractor_text` (`id`, `field`, `pattern`, `mine_ref`, `post_treatment`) VALUES
(1, 'title', 'article h1[itemprop=\"headline\"]', 1, 'result.text_content()'),
(2, 'header', 'article p[itemprop=\"about\"]', 1, 'result.text_content()'),
(3, 'text', 'article div[itemprop=\"articleBody\"]', 1, 'result.text_content()'),
(4, 'author', 'p[rel=\"author\"]', 1, 'result.text_content()'),
(5, 'pub_date', '.fig-date-pub time', 1, 'result.get(\'datetime\')'),
(6, 'article_link', 'article a', 2, 'self.rel2absURL(result.get(\'href\'), \'https://news.google.com/\')'),
(7, 'title', 'article header h1', 3, 'result.text_content()'),
(8, 'header', 'article .rtbf-article-main__content p:first-child', 3, 'result.text_content()'),
(9, 'text', 'article .rtbf-article-main__content', 3, 'result.text_content()'),
(10, 'author', '.rtbf-article-main__author span[itemprop=name]', 3, 'result.text_content()'),
(11, 'pub_date', '.rtbf-article-main__author meta[itemprop=datePublished]', 3, 'result.get(\'content\')'),
(12, 'title', 'article h1', 4, 'result.text_content()'),
(13, 'header', 'article div.lead', 4, 'result.text_content()'),
(14, 'text', 'article div.tsr-gallery', 4, 'result.text_content()'),
(98, 'title', 'header h1', 22, 'result.text'),
(16, 'pub_date', 'article span.date', 4, 'self.convertTime(str(result.text), \'%d.%m.%Y %H:%M\')'),
(17, 'title', 'h1', 5, 'result.text_content()'),
(18, 'header', '#article-text > b', 5, 'result.text'),
(19, 'text', '#article-text', 5, 'result.text_content()'),
(20, 'author', 'span.article-productionData-author', 5, 'result.text'),
(21, 'pub_date', 'span.article-productionData-date time:first-child', 5, 'result.get(\'datetime\')'),
(22, 'title', 'article h1', 6, 'result.text'),
(23, 'text', 'article .content-article-body', 6, 'result.text_content()'),
(24, 'author', 'article .content-source', 6, 'result.get(\'data-source\')'),
(25, 'pub_date', 'article time[itemProp=\'datePublished\']', 6, 'result.get(\'datetime\')'),
(26, 'title', 'article h1', 7, 'result.text'),
(27, 'header', 'article div.bd h2', 7, 'result.text_content()'),
(28, 'text', 'article div.bd', 7, 'result.text_content()'),
(29, 'author', 'article header p.author a', 7, 'result.text'),
(30, 'pub_date', 'article p.modification', 7, 'self.convertTime(result.text, \'Dernière modification : %d/%m/%Y\')'),
(31, 'title', 'h1#articleDetailTitle', 8, 'result.text'),
(32, 'header', 'article #detail_content p.intro', 8, 'result.text_content()'),
(33, 'text', 'article section.clear', 8, 'result.text_content()'),
(34, 'author', 'article span.author a[rel=\"author\"]', 8, 'result.text'),
(35, 'pub_date', 'meta[property=\"article:published_time\"]', 8, 'result.get(\'content\')'),
(36, 'title', '#article-header meta[itemprop=\"name\"]', 9, 'result.get(\'content\')'),
(37, 'header', '#article-header > p', 9, 'result.text_content()'),
(38, 'text', '#article-content', 9, 'result.text_content()'),
(39, 'author', '#article-author strong', 9, 'result.text'),
(40, 'pub_date', '#article-info meta', 9, 'result.get(\'content\')'),
(41, 'title', 'article h1', 10, 'result.text_content()'),
(42, 'header', '#entete_article h2', 10, 'result.text'),
(43, 'text', '.article > span[itemprop=\"articleBody\"]', 10, 'result.text_content()'),
(44, 'author', '#standard strong', 10, 'result.text_content()'),
(45, 'pub_date', 'article time[itemprop=\"datePublished\"]', 10, 'result.get(\'datetime\')'),
(46, 'title', 'article header h1', 11, 'result.text'),
(47, 'header', 'article .description h2', 11, 'result.text'),
(48, 'text', '#article-body', 11, 'result.text_content()'),
(49, 'author', 'article header span.author a', 11, 'result.text_content()'),
(50, 'pub_date', 'article time[itemProp=\"datePublished\"]', 11, 'result.get(\'datetime\')'),
(51, 'title', 'h2.news-title', 12, 'result.text'),
(52, 'text', 'div.news-body p:not(p[class])', 12, 'self.joinResults(result.text_content())'),
(53, 'author', 'div.news-toolbar', 12, 'self.regex(result.text, \'(.+) le \')'),
(54, 'pub_date', 'div.news-toolbar', 12, ' self.convertTime(self.regex(result.text, \'\\d+/\\d+/\\d+.*\\d+:\\d+\'), \'%d/%m/%Y à %H:%M\')'),
(55, 'title', 'article header h1', 13, 'result.text'),
(56, 'header', 'article header h2', 13, 'result.text_content()'),
(57, 'text', '.article_container p', 13, 'result.text_content()'),
(58, 'author', '.authors span[itemprop=\"author\"]', 13, 'result.text'),
(59, 'pub_date', '.authors time[itemprop=\"datePublished\"]', 13, 'result.get(\'datetime\')'),
(60, 'title', 'h1[itemprop=\"name\"]', 14, 'result.text'),
(61, 'header', 'h2[itemprop=\"headline\"]', 14, 'result.text'),
(62, 'text', 'article div[itemprop=\"articleBody\"]', 14, 'result.text_content()'),
(63, 'author', 'article .art-auteur a[rel=\"author\"]', 14, 'result.get(\'title\')'),
(64, 'pub_date', 'time[itemprop=\"datePublished\"]', 14, 'result.get(\'datetime\')'),
(65, 'title', '.posts h1', 15, 'result.text'),
(66, 'header', '.post-content > strong', 15, 'result.text_content()'),
(67, 'text', '.post-content', 15, 'result.text_content()'),
(68, 'author', '.dateheure a', 15, 'result.text'),
(69, 'pub_date', '.dateheure', 15, 'self.convertTime(result.text, \'%d %B %Y :: %H:%M :: Par \', \'fr_FR.utf8\')'),
(70, 'title', 'h1.article-header-title', 16, 'result.text'),
(71, 'header', '.article-header p', 16, 'result.text_content()'),
(72, 'text', '.article-main-text', 16, 'result.text_content()'),
(73, 'author', '.article-main-authors a', 16, 'result.text'),
(74, 'pub_date', '.article-header-date', 16, 'self.convertTime(self.regex(result.text, \'Article publié le : (.+ \\d+ .+ \\d+ à \\d+:\\d+)\'), \'%A %d %B %Y à %H:%M\', \'fr_FR.utf8\')'),
(75, 'title', 'article header h1', 17, 'result.text'),
(76, 'header', 'article header div.lead', 17, 'result.text_content()'),
(77, 'text', 'article div.gr-article-content', 17, 'result.text_content()'),
(78, 'author', '.gr-meta-author', 17, 'result.text_content()'),
(79, 'pub_date', '.gr-section-meta time', 17, 'result.get(\'datetime\')'),
(80, 'title', 'article h3.header', 18, 'result.text'),
(81, 'header', 'article header p.desc', 18, 'result.text_content()'),
(82, 'text', 'article div.content-txt', 18, 'result.text_content()'),
(83, 'pub_date', 'article header p.datePublished time', 18, 'result.get(\'datetime\')'),
(84, 'title', 'article.article header.article__header h1', 19, 'result.text_content()'),
(85, 'header', 'article.article .article__body p.article__intro', 19, 'result.text_content()'),
(86, 'text', 'article.article .article__body p', 19, 'self.joinResults(result.text_content())'),
(87, 'author', 'article.article header span[itemprop=\"source\"]', 19, 'result.text_content()'),
(88, 'pub_date', 'meta[property=\"article:modified_time\"]', 19, 'self.convertTime(result.get(\'content\')[:18], \'%Y-%m-%dT%H:%M:%S\')'),
(89, 'title', 'div#main h1', 20, 'result.text'),
(90, 'header', 'div#main p.intro', 20, 'result.text_content()'),
(91, 'text', 'div#main div#content-article', 20, 'result.text_content()'),
(92, 'author', 'div#main p.author', 20, 'result.text_content()'),
(93, 'pub_date', 'div#main div.date', 20, 'self.convertTime(result.text.strip(), \'Le %d/%m/%Y à %H:%M:%S\')'),
(94, 'title', 'article h1', 21, 'result.text_content()'),
(95, 'header', 'article h2[itemprop=\"headline\"]', 21, 'result.text_content()'),
(96, 'text', 'article #body-detail', 21, 'result.text_content()'),
(97, 'pub_date', 'article span.update-date', 21, 'self.convertTime(\'08 août 2017 à 08h11\', \'%d %B %Y à %Hh%M\', \'fr_FR.utf8\')'),
(99, 'header', '.field-name-field-standfirst', 22, 'result.text_content()'),
(100, 'text', 'article .field-items', 22, 'result.text_content()'),
(101, 'author', '.byline-author span', 22, 'result.text_content()'),
(102, 'pub_date', '.byline p', 22, 'self.convertTime(self.regex(result.text_content().strip(), \'Publié le (\\d+/\\d+/\\d+ à \\d+:\\d+)\'), \'%d/%m/%Y à %H:%M\', \'fr_FR.utf8\')'),
(103, 'title', 'h1.article_title', 23, 'result.text_content()'),
(104, 'header', 'div#article-text b', 23, 'result.text_content()'),
(105, 'text', 'div#article-text', 23, 'result.text_content()'),
(106, 'author', 'span.article-productionData-author', 23, 'result.text_content()'),
(107, 'pub_date', '.article-productionData-date time:nth-child(1)', 23, 'result.get(\'datetime\')'),
(108, 'article_link', 'noscript a', 24, 'result.get(\'href\')'),
(109, 'header', '.description-article', 6, 'result.text_content()'),
(110, 'title', 'article h1', 25, 'result.text_content()'),
(111, 'header', 'article div.lead', 25, 'result.text_content()'),
(112, 'text', 'article div.gr-article-content', 25, 'result.text_content()'),
(113, 'author', '.gr-meta-author', 25, 'result.text_content()'),
(114, 'pub_date', '.gr-article-infos-content time', 25, 'result.get(\'datetime\')'),
(115, 'title', 'article h1', 26, 'result.text_content()'),
(116, 'header', '.td-post-content p strong', 26, 'result.text_content()'),
(117, 'text', '.td-post-content p', 26, 'self.joinResults(result.text_content())'),
(118, 'author', '.td-post-source-via a', 26, 'result.text_content()'),
(119, 'pub_date', 'article .td-module-meta-info .td-post-date time', 26, 'self.convertTime(result.get(\'datetime\'), \'%Y-%m-%dT%H:%M:%S+00:00\')'),
(120, 'title', 'article h1', 27, 'result.text_content()'),
(121, 'header', 'p.entete', 27, 'result.text_content()'),
(122, 'text', 'div[itemprop=\"articleBody\"]', 27, 'result.text_content()'),
(123, 'author', 'div[itemprop=\"author\"] span[itemprop=\"name\"]', 27, 'result.text_content()'),
(124, 'pub_date', '.publication time[itemprop=\"datePublished\"]', 27, 'self.convertTime(result.get(\'datetime\')[:18], \'%Y-%m-%dT%H:%M:%S\')'),
(125, 'title', 'article h1', 28, 'result.text_content()'),
(126, 'header', 'h2.article-chapo', 28, 'result.text_content()'),
(127, 'text', '.article-mask p', 28, 'self.joinResults(result.text_content())'),
(128, 'author', '.article-social-author-name', 28, 'result.text_content()'),
(129, 'pub_date', '.article-info time', 28, 'self.convertTime(result.get(\'datetime\')[:18], \'%Y-%m-%dT%H:%M:%S\')'),
(130, 'title', 'article h1', 29, 'result.text_content()'),
(131, 'header', 'div.chapo', 29, 'result.text_content()'),
(132, 'text', 'article div.wrap p', 29, 'self.joinResults(result.text_content())'),
(133, 'author', 'div.author a', 29, 'result.text_content()'),
(134, 'pub_date', 'div.time.published_at', 29, 'self.convertTime(result.text_content()[:14], \'%d/%m/%y %Hh%M\')'),
(135, 'title', 'article h1', 30, 'result.text_content()'),
(136, 'header', 'article div.lead', 30, 'result.text_content()'),
(137, 'text', 'article div.gr-article-content p', 30, 'self.joinResults(result.text_content())'),
(138, 'pub_date', '.gr-article-infos-content time', 30, 'self.convertTime(result.get(\'datetime\')[:18], \'%Y-%m-%dT%H:%M:%S\')'),
(139, 'title', 'article header h1', 31, 'result.text_content()'),
(140, 'header', 'article .o-post__content p:nth-child(2)', 31, 'result.text_content()'),
(141, 'text', 'article .o-post__content p', 31, 'self.joinResults(result.text_content())'),
(142, 'author', 'article header .c-post__author a.author', 31, 'result.text_content()'),
(143, 'pub_date', 'article time.post-created-time', 31, 'self.convertTime(result.get(\'datetime\')[:18], \'%Y-%m-%dT%H:%M:%S\')');

-- --------------------------------------------------------

--
-- Table structure for table `img`
--

CREATE TABLE `img` (
  `id` int(11) NOT NULL,
  `src_url` text NOT NULL,
  `local_url` text NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `article_ref` int(10) UNSIGNED NOT NULL,
  `caption` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imgs_rel`
--

CREATE TABLE `imgs_rel` (
  `a_ref` int(10) UNSIGNED NOT NULL,
  `b_ref` int(10) UNSIGNED NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `img_meta`
--

CREATE TABLE `img_meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `field` varchar(100) NOT NULL,
  `data1` text NOT NULL,
  `data2` text NOT NULL,
  `data3` text NOT NULL,
  `ref_img` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `source`
--

CREATE TABLE `source` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(60) NOT NULL,
  `url` text NOT NULL,
  `country` varchar(3) NOT NULL,
  `charset` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL DEFAULT 'source',
  `depth` int(11) NOT NULL DEFAULT '1',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `method` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `source`
--

INSERT INTO `source` (`id`, `name`, `url`, `country`, `charset`, `type`, `depth`, `active`, `method`) VALUES
(1, 'Le figaro', 'www.lefigaro.fr', 'FRA', 'utf-8', 'source', 1, 1, ''),
(2, 'Google news be fr', 'https://news.google.com/?hl=fr&gl=BE&ceid=BE:fr', 'BEL', 'utf-8', 'aggregator', 2, 1, ''),
(3, 'RTBF', 'www.rtbf.be', 'BEL', 'utf-8', 'source', 1, 1, ''),
(4, 'RTS info', 'www.rts.ch', 'CHE', 'utf-8', 'source', 1, 1, ''),
(5, 'La Libre', 'www.lalibre.be', 'BEL', 'utf-8', 'source', 1, 1, ''),
(6, 'Le Monde', 'www.lemonde.fr', 'FRA', 'utf-8', 'source', 1, 1, ''),
(7, 'France24', 'www.france24.com', 'FRA', 'utf-8', 'source', 1, 1, ''),
(8, '7sur7', 'www.7sur7.be', 'BEL', 'utf-8', 'source', 1, 1, 'br.select_form(nr=0)\r\ntry:\r\n    data = br.submit()\r\nexcept mechanize.HTTPError:\r\n    response = br.response()\r\n    http_header_dict = response.info().dict\r\n    data = br.open(http_header_dict[\'location\'])'),
(9, 'Le JDD', 'www.lejdd.fr', 'FRA', 'utf-8', 'source', 1, 1, ''),
(10, 'Le Point', 'www.lepoint.fr', 'FRA', 'utf-8', 'source', 1, 1, ''),
(11, 'Libération', 'www.liberation.fr', 'FRA', 'utf-8', 'source', 1, 1, ''),
(12, 'Boursorama', 'www.boursorama.com', 'FRA', 'ISO-8859-1', 'source', 1, 1, ''),
(13, 'L\'express', 'www.lexpress.fr', 'FRA', 'iso-8859-1', 'source', 1, 1, ''),
(14, 'le nouvel observateur', 'tempsreel.nouvelobs.com', 'FRA', 'utf-8', 'source', 1, 1, ''),
(15, 'Presse Citron', 'www.presse-citron.net', 'FRA', 'utf-8', 'source', 1, 1, ''),
(16, 'RFI', 'www.rfi.fr', 'FRA', 'utf-8', 'source', 1, 1, ''),
(17, 'Sud info', 'www.sudinfo.be', 'BEL', 'utf-8', 'source', 1, 1, ''),
(18, 'Digimedia', 'www.digimedia.be', 'BEL', 'utf-8', 'source', 1, 1, ''),
(19, 'L\'avenir', 'www.lavenir.net', 'BEL', 'utf-8', 'source', 1, 1, ''),
(20, 'Réponse à tout', 'www.reponseatout.com', 'BEL', 'utf-8', 'source', 1, 1, ''),
(21, 'RTL', 'www.rtl.be', 'BEL', 'utf-8', 'source', 1, 1, ''),
(22, 'Le Figaro Bourse', 'bourse.lefigaro.fr', 'FRA', 'utf-8', 'source', 1, 1, ''),
(23, 'Dhnet', 'www.dhnet.be', 'BEL', 'utf-8', 'source', 1, 1, ''),
(24, 'google news redirection', 'news.google.com', 'NUL', 'UTF-8', 'source', 1, 1, ''),
(25, 'Le Soir', 'www.lesoir.be', 'BE', 'utf-8', 'source', 1, 1, ''),
(26, 'Metrotime', 'fr.metrotime.be', 'BEL', 'utf-8', 'source', 1, 1, ''),
(27, 'Paris Match', 'www.parismatch.com', 'FRA', 'utf-8', 'source', 1, 1, ''),
(28, 'RTL', 'www.rtl.fr', 'FRA', 'utf-8', 'source', 1, 1, ''),
(29, 'Les Inrocks', 'www.lesinrocks.com', 'FRA', 'utf-8', 'source', 1, 1, ''),
(30, 'Le Soir Mag', 'soirmag.lesoir.be', 'BEL', 'utf-8', 'source', 1, 1, ''),
(31, 'Paris Match Belgique', 'parismatch.be', 'BEL', 'utf-8', 'source', 1, 1, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mine_ref` (`mine_ref`);

--
-- Indexes for table `extractor_img`
--
ALTER TABLE `extractor_img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `extractor_text`
--
ALTER TABLE `extractor_text`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `img`
--
ALTER TABLE `img`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_ref` (`article_ref`);

--
-- Indexes for table `img_meta`
--
ALTER TABLE `img_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref_img` (`ref_img`);

--
-- Indexes for table `source`
--
ALTER TABLE `source`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=474301;

--
-- AUTO_INCREMENT for table `extractor_img`
--
ALTER TABLE `extractor_img`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `extractor_text`
--
ALTER TABLE `extractor_text`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `img`
--
ALTER TABLE `img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=394137;

--
-- AUTO_INCREMENT for table `img_meta`
--
ALTER TABLE `img_meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=446112;

--
-- AUTO_INCREMENT for table `source`
--
ALTER TABLE `source`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
