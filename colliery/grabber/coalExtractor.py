#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import datetime
import locale
from cssselect import HTMLTranslator
import cssselect
#from lxml.etree import fromstring
from lxml import etree
import re
from logger import logger
from urlparse import urlparse

class CoalExtractor(object):
    def __init__(self, params, charset):
        try:
            self.id = params['id']
            self.field = params['field']
            self.pattern = params['pattern']
            self.treatment = params['post_treatment']
            self.results = []
            self.charset = charset
            self.finalJoin = False

        except KeyError, e:
            logger.exception("coalExtractor error : %s is not in the parameters." % sys.exc_value)

    def extract(self, src):

        for selector in cssselect.parse(self.pattern):

            expression = HTMLTranslator().selector_to_xpath(selector, translate_pseudo_elements=True)

            for result in src.xpath(expression):

                try:
                    #logger.info("data extracted before post-treatment: %s" % etree.tostring(result))
                    res = eval(self.treatment).strip()
                    if(res != ''):
                        self.results.append(res)
                except Exception, e:
                    #log
                    logger.exception(e)

        if(self.finalJoin):
            self.results = [" ".join(self.results)]

    def addPrefix(self, value, prefix):
        prefix = prefix.decode('utf-8')
        return prefix+value

    def addPostFix(self, value, postfix):
        postfix = postfix.decode('utf-8')
        return value+postfix
    def rel2absURL(self, value, rootURL):
        return rootURL + value.lstrip("./")

    def contentWithoutElements(self, value, pattern):
        for selector in cssselect.parse(pattern):
            expression = HTMLTranslator().selector_to_xpath(selector, translate_pseudo_elements=True)
            #logger.info('trying to launch this function %s', selector)
            for elem in value.xpath(expression):
                elem.getparent().remove(elem)

        return value.text_content()

    def lastFromSrcSet(self, value):
        items = value.split(',');
        return items[len(items)-1]

    def removeQueryString(self, value):
        o = urlparse(value)
        return o.scheme + "://" + o.netloc + o.path



    def regex(self, value, regex):
        #logger.info(value)
        #logger.info(regex)
        p = re.compile(regex.decode('utf-8'))
        results = p.findall(value)
        #logger.info(results)

        if(len(results)>0):
            return results[0]
        else:
             return ''

class TextExtractor(CoalExtractor):
    def __init__(self, params, charset):
        super(TextExtractor, self).__init__(params, charset)
        self.medium = 'text'

    def convertTime(self, value, format, loc=''):
        locale.setlocale(locale.LC_ALL, loc)

        if(isinstance(value, str)):
            value = value.decode(self.charset).encode('utf-8')
        else:
            value = value.encode('utf-8')

        try:
            result = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(value, format))
        except Exception, e:
            #log
            error = e
            logger.exception(error)

        locale.setlocale(locale.LC_ALL, '')
        return(result.decode('utf-8'))


    def joinResults(self, value):
          self.finalJoin = True
          return value

class ImgExtractor(CoalExtractor):
    def __init__(self, params, charset):
        super(ImgExtractor, self ).__init__(params, charset)
        self.medium = 'image'
        self.patternCaption = params['pattern_caption']
        self.treatmentCaption = params['post_treatment_caption']
        self.captions = []

    def extract(self, src):
        super(ImgExtractor, self).extract(src)
        if(self.patternCaption == ''):
            return
        for selector in cssselect.parse(self.patternCaption):
            expression = HTMLTranslator().selector_to_xpath(selector)

            for result in src.xpath(expression):
                try:
                    self.captions.append(str(eval(self.treatmentCaption).encode('utf-8')).strip())
                except Exception as e:
                    logger.exception(e)
