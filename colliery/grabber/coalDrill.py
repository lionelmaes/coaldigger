#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

from coalMine import *
from logger import logger

class CoalDrill:
    def __init__(self, db):
        self.aggregators = []
        self.sources = []
        self.db = db
        aggrRefs = db.getAggregators()

        for aggrRef in aggrRefs:
            logger.info('STEP 1: AGGREGATORS DRILLING\n')
            logger.info(aggrRef)
            self.aggregators.append(CoalMine('aggregator', aggrRef, db))



    def drill(self):

        articleLinks = []
        for aggregator in self.aggregators:
            currentMines = [aggregator]
            for i in range(0, aggregator.depth):
                currentLinks = []
                logger.info('drilling aggregator %s with depth %d\n' % (aggregator.name, i))
                for currentMine in currentMines:
                    currentLinks.extend(list(set(self.loadArticlesLinks(currentMine))))
                #logger.debug(currentLinks)

                if i < (aggregator.depth - 1):

                    currentMines = []
                    logger.debug("drill deeper")
                    i = 0
                    for currentLink in currentLinks:
                        currentMines.append(CoalMine('source', {'url':currentLink}, self.db))
                        if i > 20:
                            break
                        i += 1

                else:
                    articleLinks.extend(currentLinks)


        #articleLinks = list(set(self.loadArticlesLinks()))
        logger.info('STEP 2: ARTICLES DRILLING\n')
        for articleLink in articleLinks:

            try:
                if(not(self.db.inDB(articleLink))):
                    mine = CoalMine('source', {'url':articleLink}, self.db)
                    if(mine.drillable):
                        self.sources.append(mine)
                        mine.extract()
                        mine.save()
            except Exception, e:
                logger.exception(e)


    def drillMine(self, url):
        mine = CoalMine('source', {'url':url}, self.db)
        if(mine.drillable):
            mine.extract()
            mine.save()

    def loadArticlesLinks(self, aggregator):
        links = []
        aggregator.extract()
        for extractor in aggregator.extractors:

            if(extractor.field == 'article_link'):
                links += extractor.results
        return links
