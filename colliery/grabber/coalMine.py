#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import mechanize
from bs4 import BeautifulSoup
import lxml.html as lh
from urlparse import urlparse
import time
import os
import uuid
import itertools
from mimetypes import guess_extension
import socket
import config
from coalExtractor import *
from PIL import Image
from logger import logger

class CoalMine:
    def __init__(self, type, params, db):

        self.url = params['url']

        self.drillable = True
        self.type = type

        if(type == 'aggregator'):
            self.id = params['id']
            self.depth = params['depth']
            self.name = params['name']
            self.charset = params['charset']
            self.country = params['country']
            self.method = params['method']
            logger.debug('depth: %d' % self.depth)
        else:
            urlparsing = urlparse(self.url)
            self.baseurl = urlparsing.netloc
            source = db.getSource(self.baseurl)

            if(source == -1):
                with open(config.missingLogs, 'a') as f:
                    f.write(self.url+' '+self.baseurl+'\n')
                logger.info('missing source %s with article %s\n' % (self.baseurl, self.url))
                self.drillable = False
                return
            else:
                self.id = source['id']
                self.name = source['name']
                self.charset = source['charset']
                self.country = source['country']
                self.depth = source['depth']
                self.method = source['method']

        self.db = db
        self.loadExtractors()
        self.loadSrc()

    def loadExtractors(self):
        textPatterns = self.db.getExtractors(self.id, 'extractor_text')
        imgPatterns = self.db.getExtractors(self.id, 'extractor_img')
        self.extractors = []
        for pattern in textPatterns:
            self.extractors.append(TextExtractor(pattern, self.charset))
        for pattern in imgPatterns:
            self.extractors.append(ImgExtractor(pattern, self.charset))

    def loadSrc(self):
        logger.info('mining: %s\n' % self.url)
        logger.debug('url: %s, headers: %s' % (self.url, config.crawlerHeaders))
        br = mechanize.Browser()
        br.addheaders = config.crawlerHeaders
        br.set_handle_robots(False)
        data = br.open(self.url, timeout=10.0)

        ##insert specific methods here
        if self.method != '':
            exec(self.method)

        rawdata = data.read()
        unicode = rawdata.decode(self.charset, 'ignore')
        self.src = lh.fromstring(unicode)
        #logger.debug(unicode)

    def extract(self):
        for extractor in self.extractors:
            logger.info("using extractor: %s on %s\n" % (extractor.pattern, self.url))

            extractor.extract(self.src)
            out = 'extracted: %d element(s): \n' % len(extractor.results)

            for result in extractor.results:
                out += '\n'+result

            logger.info(out)

    def save(self):
        articleData = {'title':'',
                       'header':'',
                       'text':'',
                       'pub_date':'',
                       'src_url':self.url,
                       'author':'',
                       'mine_ref':self.id
                       }
        images = []
        for extractor in self.extractors:
            if(extractor.medium == 'image'):

                for url,caption in itertools.izip_longest(extractor.results, extractor.captions, fillvalue=''):
                    if(url == ''):
                        break
                    try:
                        imageData = self.saveImage(url)
                    except Exception, e:
                        continue
                    images.append({'src_url':url, 'local_url':imageData[0], 'width':imageData[1], 'height':imageData[2], 'caption':caption})
            elif(articleData.has_key(extractor.field)):
                articleData[extractor.field] = '/-//-/'.join(extractor.results)
        #ici on va sauver les donn�es
        # print articleData
        #print images
        if(articleData['title'] == '' and articleData['text'] == ''):
            logger.info('no title, no text = no article. Check the extractors for %s' % self.baseurl)
            return

        articleId = self.db.saveArticle(articleData)
        for imageData in images:
            self.db.saveImage(imageData, articleId)


    def saveImage(self, imgUrl):
        logger.info('saving image %s\n' % imgUrl)
        directory = config.dirname % time.localtime()[0:3]
        #print directory

        if not os.path.exists(directory):
            os.makedirs(directory)
        CHUNK = 16 * 1024
        #print imgUrl

        br = mechanize.Browser()
        br.addheaders = config.crawlerHeaders
        br.set_handle_robots(False)
        response = br.open(imgUrl, timeout=10.0)

        type = response.info().getheader('Content-Type')
        file = directory + '/' + str(uuid.uuid4()) + guess_extension(type)

        with open(file, 'wb') as fp:
            for chunk in iter(lambda:response.read(CHUNK), ''):
                fp.write(chunk)


        im = Image.open(file)
        width, height = im.size
        return (file, width, height)
