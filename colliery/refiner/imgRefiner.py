#!/usr/bin/python
# -*- coding: utf-8 -*-
import colorsys
from PIL import Image

class ImgRefiner:
    def __init__(self, db):
        self.db = db

    def loadImages(self):
        self.images = self.db.getImagesToRefine('hsl')

    def hslTreatment(self):
        for image in self.images:
            try:
                
                im = Image.open(image['local_url'])
                if im.mode != 'RGB':
                    im = im.convert('RGB')
                im = im.resize((1, 1))
                px = im.load()
                r, g, b = px[0, 0]
                h,l,s = colorsys.rgb_to_hls(r/255.,g/255.,b/255.)
                self.db.saveRefine({'field':'hsl', 'ref_img':image['id'], 'data1':h*360, 'data2':s*100, 'data3':l*100})

            except Exception, e:
                print e
