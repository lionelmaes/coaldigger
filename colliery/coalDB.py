﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb as mdb
import sys

import config
from logger import logger

class CoalDB:
    def __init__(self):
        self.conn = mdb.connect(config.dbServer, config.dbUser, config.dbPassword, config.dbName, charset = "utf8", use_unicode = True)

    def getAggregators(self):
		return self.getMines('aggregator')

    def getMines(self, type = -1):
        with self.conn:
            cur = self.conn.cursor(mdb.cursors.DictCursor)
            if(type != -1):
                cur.execute("SELECT * FROM source WHERE active=%s AND type=%s", (1, type))
            else:
                cur.execute("SELECT * FROM source")
            rows = cur.fetchall()

            return rows

    def getExtractors(self, mineId, table):

        with self.conn:
            cur = self.conn.cursor(mdb.cursors.DictCursor)

            cur.execute("SELECT * FROM "+ table +" WHERE mine_ref=%s", (mineId,))

            rows = cur.fetchall()

            return rows

    def getExtractor(self, mineId, table, field):
        with self.conn:
            cur = self.conn.cursor(mdb.cursors.DictCursor)

            cur.execute("SELECT * FROM "+ table +" WHERE mine_ref=%s AND field=%s", (mineId, field))
            rows = cur.fetchall()

            if(len(rows) == 0):
                return -1
            else:
                return rows[0]

    def getSource(self, baseurl):
        with self.conn:
            cur = self.conn.cursor(mdb.cursors.DictCursor)
            cur.execute("SELECT id, name, country, charset, depth, method FROM source WHERE url = %s", (baseurl,))
            rows = cur.fetchall()

            if(len(rows) == 0):
                return -1
            else:
                return rows[0]

    def getImagesToRefine(self, treatment):
        with self.conn:
            cur = self.conn.cursor(mdb.cursors.DictCursor)
            cur.execute("SELECT DISTINCT img.id, img.local_url FROM img LEFT JOIN img_meta ON img.id = img_meta.ref_img WHERE field != %s OR img_meta.ref_img is null", (treatment,))
            rows = cur.fetchall()
            return rows


    def inDB(self, url):
        with self.conn:
            cur = self.conn.cursor()
            cur.execute("SELECT id FROM article WHERE src_url = %s", (url,))
            rows = cur.fetchall()
            if(len(rows) == 0):
                return False
            else:
                return True


    def saveArticle(self, articleData):
        with self.conn:
            cur = self.conn.cursor()
            #print articleData['pub_date']
            cur.execute("INSERT INTO article "
                        "(title, header, text, insert_date, pub_date, src_url, author, mine_ref) "
                        "VALUES (%(title)s, %(header)s, %(text)s, NOW(), %(pub_date)s, %(src_url)s, %(author)s, %(mine_ref)s) ",
                        articleData)
            return cur.lastrowid

    def saveImage(self, imageData, articleId):
        with self.conn:
            cur = self.conn.cursor()
            cur.execute("INSERT INTO img "
                        "(src_url, local_url, width, height, article_ref, caption) "
                        "VALUES (%(src_url)s, %(local_url)s, %(width)s, %(height)s, "+str(articleId)+", %(caption)s) ",
                        imageData)


    def saveSource(self, name, url, country, charset, method = '', overwrite = True):
        source = self.getSource(url)
        if source != -1:
            if overwrite:
                with self.conn:
                    cur = self.conn.cursor()
                    cur.execute("UPDATE source SET name = %s, country = %s, charset = %s, method = %s WHERE url = %s",
                    (name, country, charset, method, url))
                return source['id']
            else:
                return False
        with self.conn:
            cur = self.conn.cursor()

            cur.execute("INSERT INTO source "
                        "(name, url, country, charset, method) "
                        "VALUES (%s, %s, %s, %s, %s)",
                        (name, url, country, charset, method))
            return cur.lastrowid


    def saveTextExtractor(self, mineId, field, pattern, postTreatment, overwrite = True):

        if self.getExtractor(mineId, "extractor_text", field) != -1:
            if overwrite:
                with self.conn:
                    cur = self.conn.cursor()

                    cur.execute("UPDATE extractor_text "
                                "SET pattern = %s, post_treatment = %s "
                                "WHERE mine_ref = %s AND field = %s",
                                (pattern, postTreatment, mineId, field))
                return True
            else:
                return False

        with self.conn:
            cur = self.conn.cursor()
            cur.execute("INSERT INTO extractor_text "
                        "(field, pattern, mine_ref, post_treatment) "
                        "VALUES (%s, %s, %s, %s)",
                        (field, pattern, mineId, postTreatment))


    def saveImageExtractor(self, mineId, field, pattern, postTreatment, caption, postCaption, overwrite = True):
        if self.getExtractor(mineId, "extractor_img", field) != -1:
            if overwrite:
                with self.conn:
                    cur = self.conn.cursor()
                    cur.execute("UPDATE extractor_img "
                                "SET pattern = %s, post_treatment = %s, pattern_caption = %s, post_treatment_caption = %s "
                                "WHERE mine_ref = %s AND field = %s",
                                (pattern, postTreatment, caption, postCaption, mineId, field))
                return True
            else:
                return False

        with self.conn:
            cur = self.conn.cursor()
            cur.execute("INSERT INTO extractor_img "
                        "(field, pattern, pattern_caption, mine_ref, post_treatment, post_treatment_caption) "
                        "VALUES (%s, %s, %s, %s, %s, %s)",
                        (field, pattern, caption, mineId, postTreatment, postCaption))

    def saveRefine(self, values):
            with self.conn:
                cur = self.conn.cursor()
                cur.execute("INSERT INTO img_meta "
                            "(field, ref_img, data1, data2, data3) "
                            "VALUES (%(field)s, %(ref_img)s, %(data1)s, %(data2)s, %(data3)s)",
                            values)
