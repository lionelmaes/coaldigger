# -*- coding: utf-8 -*-
import json
import os

##DB CONFIG
dbServer = "localhost"
dbName = "replace with your db name"
dbUser = "replace with your user name"
dbPassword = "replace with the user password"

##CRAWLER SETTINGS
crawlerHeaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

##LOGS
logLevel = 'DEBUG'

##LOG FILES PATHS
errorLogs = "replace with the path to the log folder/error.log"
debugLogs = "replace with the path to the log folder/debug.log"
infoLogs = "replace with the path to the log folder/info.log"
missingLogs = "replace with the path to the log folder/missing.log"

##IMGS PATHS
dirname = "replace with the path to the data folder/%04d%02d%02d"
