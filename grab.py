#!/usr/bin/python
# -*- coding: utf-8 -*-

from colliery.coalDB import *
from colliery.grabber.coalDrill import *
from logger import logger
import config

logger.info('#BEGIN COALDIGGER#\n')
db = CoalDB()
coalDrill = CoalDrill(db)
coalDrill.drill()
logger.info('#END COALDIGGER (waiting for relaunch)#\n')
#coalDrill.drillMine('https://www.7sur7.be/7s7/fr/1506/Sciences/article/detail/3458841/2018/07/31/Mars-n-a-plus-ete-aussi-proche-de-la-Terre-depuis-15-ans.dhtml')
