#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import config
from logging.handlers import RotatingFileHandler


logger = logging.getLogger()
logger.setLevel(config.logLevel)


formatter = logging.Formatter('%(asctime)s :: %(module)s :: %(lineno)d :: %(levelname)s :: %(message)s')


errorHandler = RotatingFileHandler(config.errorLogs, 'a', 1000000, 1)
debugHandler = RotatingFileHandler(config.debugLogs, 'a', 1000000, 1)
infoHandler = RotatingFileHandler(config.infoLogs, 'a', 1000000, 1)

errorHandler.setLevel('ERROR')
errorHandler.setFormatter(formatter)

logger.addHandler(errorHandler)

debugHandler.setLevel('DEBUG')
debugHandler.setFormatter(formatter)

logger.addHandler(debugHandler)

infoHandler.setLevel('INFO')
infoHandler.setFormatter(formatter)

logger.addHandler(infoHandler)

steamHandler = logging.StreamHandler()
steamHandler.setLevel(config.logLevel)
logger.addHandler(steamHandler)
