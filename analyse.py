#!/usr/bin/python
# -*- coding: utf-8 -*-

from colliery.coalDB import *
from colliery.refiner.imgRefiner import *

db = CoalDB()
imgRefiner = ImgRefiner(db)
imgRefiner.loadImages()
imgRefiner.hslTreatment()

